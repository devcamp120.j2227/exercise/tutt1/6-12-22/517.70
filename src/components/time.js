import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

class Time extends Component {
    hour = new Date().getHours();
    minute = new Date().getMinutes();
    second = new Date().getSeconds();

    changeColor = () => {
        return this.second %2 ? "text-danger" : "text-primary"
    }
    render() {
        return (
            <div className="container text-center mt-5">
                <h1 className={this.changeColor()}>Hello, world</h1>
                <h3>It is {this.hour}:{this.minute}:{this.second} {this.hour <= 12 ? "AM" : "PM"}</h3>
                <button className="btn btn-success mt-4" onClick={() => this.changeColor()}>Change color</button>
            </div>
        )
    }
}

export default Time